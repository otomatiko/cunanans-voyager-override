<style type="text/css">
		
		.voyager .side-menu.sidebar-inverse .navbar li>a {
			color: #302f2f;
			font-family: 'Lato', sans-serif;
		}

		.voyager .side-menu.sidebar-inverse .navbar li>a>.icon {
			color: #C72D41;
		}

		.sidebar-inverse {
			background: #d6dbd7 !important;
		}

		.logo-link-container {
			height: 20vh !important;
			padding-top: 2.5vh;
			padding-bottom: 2.5vh;
			padding-left: 0.5vw;
		}

		.logo-link {
			height: 100%;
		}

		.logo-container {
			height: 100%;
		}

		.my_logo {
			height: 100%;
		}





		.logo-link-container-small {
			height: 7.5vh !important;
			padding-left: 0.7vw;
			padding-top: 0.5vw;
			padding-bottom: 0.5vw;
		}

		.logo-link-small {
			height: 100%;
		}

		.logo-container-small {
			height: 100%;
		}

		.my_logo_small {
			height: 100%;
		}

</style>

<div class="side-menu sidebar-inverse">
	<nav class="navbar navbar-default" role="navigation">

		<div id="logo-big" class="">
			<div class="logo-link-container">
				<a class="logo-link" href="{{ route('voyager.dashboard') }}">

					<div class="logo-container">
						<?php $admin_logo_img = Voyager::setting('admin.icon_image', ''); ?>
						@if($admin_logo_img == '')
							<img class="my_logo_small" src="{{ voyager_asset('images/logo-icon-light.png') }}" alt="Logo Icon">
							<img class="my_logo hidden" src="{{ voyager_asset('images/logo-icon-light.png') }}" alt="Logo Icon">
						@else
							<img class="my_logo" src="{{ Voyager::image($admin_logo_img) }}" alt="Logo Icon"> 
						@endif
					</div>

				</a>
			</div>
		</div>

		<div id="logo-small" class="">
			<div class="logo-link-container-small">
				<a class="logo-link-small" href="{{ route('voyager.dashboard') }}">

					<div class="logo-container-small">
						<?php $admin_logo_img = Voyager::setting('admin.icon_image', ''); ?>
						@if($admin_logo_img == '')
							<img class="my_logo_small" src="{{ voyager_asset('images/logo-icon-light.png') }}" alt="Logo Icon">
							<img class="my_logo" src="{{ voyager_asset('images/logo-icon-light.png') }}" alt="Logo Icon">
						@else
							<img class="my_logo_small" src="{{ Voyager::image($admin_logo_img) }}" alt="Logo Icon">
						@endif
					</div>

				</a>
			</div>
		</div>


		<div id="adminmenu">
			<admin-menu :items="{{ menu('admin', '_json') }}"></admin-menu>
		</div>
	</nav>
</div>

<script type="text/javascript">

  	function test(){

  		if($(".side-menu").width() == 250){
	  		
	  		// alert($(".side-menu").width());

	  		$("#logo-small").addClass("hidden");


  		}else{

  			$("#logo-big").addClass("hidden");


  		}

  	}

  	setTimeout(test, 100);


	$(".hamburger").click(function(){

		if($(".side-menu").width() == 250){
	  		
	  		// alert($(".side-menu").width());
	  		$("#logo-big").addClass("hidden");
	  		$("#logo-small").removeClass("hidden");


  		}else{

  			$("#logo-big").removeClass("hidden");
  			$("#logo-small").addClass("hidden");

  		}

	}); 


</script>
