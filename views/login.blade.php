<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" dir="{{ __('voyager::generic.is_rtl') == 'true' ? 'rtl' : 'ltr' }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="none" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="admin login">
    <title>Admin - {{ Voyager::setting("admin.title") }}</title>
    <link rel="stylesheet" href="{{ voyager_asset('css/app.css') }}">

    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300&display=swap" rel="stylesheet">

    @if (__('voyager::generic.is_rtl') == 'true')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.css">
        <link rel="stylesheet" href="{{ voyager_asset('css/rtl.css') }}">
    @endif
    <style>
        body {
            
            background-color: {{ Voyager::setting("admin.primary_color_null", "#d6dbd7" ) }};
        }
        body.login .login-sidebar {
            border-top:5px solid {{ config('voyager.primary_color_null','#d6dbd7') }};
        }
        @media (max-width: 767px) {
            body.login .login-sidebar {
                border-top:0px !important;
                border-left:5px solid {{ config('voyager.primary_color_null','#d6dbd7') }};
            }d6dbd7
        }
        body.login .form-group-default.focused{
            border-color:{{ config('voyager.primary_color_null','#d6dbd7') }};
        }
        .login-button, .bar:before, .bar:after{
            background:{{ config('voyager.primary_color_null','#302f2f') }};
        }
        .remember-me-text{
            padding:0 5px;
        }

        .login-container {
            top: 60% !important;
        }

        #emailGroup {
            border-radius: 50px;
            width: 50%;
            margin-right: auto;
            margin-left: auto;
        }

        #passwordGroup {
            border-radius: 50px;
            width: 50%;
            margin-right: auto;
            margin-left: auto;
        }

        .login-button {
            border-radius: 50px !important;
            width: 50% !important;
            margin-right: auto;
            margin-left: auto;
            float: none !important;
        }

        label {
            text-align: center;
            font-family: 'Lato', sans-serif;
        }

        input {
            border-radius: 25px 25px 25px 25px !important;
            font-family: 'Lato', sans-serif;
        }

        #logo_con {
            height: 30vh;
            position: absolute;
            transform: translate(-50%, -50%);
            top: 32%;
            left: 50%;
        }

        #logo {
            height: 100%;
            
            /*width: 100%;*/
        }

        #text-only {
            position: absolute;
            transform: translate(-50%, -50%);
            top: 120%;
            left: 50%;
        }

        #text-only-p {
            font-family: 'Lato', sans-serif !important;
            font-size: 14px !important;
            text-transform: none;
            font-weight: 300;
            letter-spacing: 1px;
        }

    </style>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
</head>
<body class="login">
<div class="container-fluid">
    <div class="row">

        <div class="col-xs-12 col-sm-5 col-md-12     login-sidebar">

            <div id="logo_con">

               <img id="logo" src="{{ asset('images/logo.png') }}" alt="Logo Icon">

            </div>

            <div class="login-container">

                <p style="text-align: center;">{{ __('voyager::login.signin_below') }}</p>

                <form action="{{ route('voyager.login') }}" method="POST" style="width: 40%;display: block; margin-left: auto; margin-right: auto">
                    {{ csrf_field() }}
                    <div class="form-group form-group-default" id="emailGroup">
                        <label>{{ __('voyager::generic.email') }}</label>
                        <div class="controls">
                            <input type="text" name="email" id="email" value="{{ old('email') }}" placeholder="{{ __('voyager::generic.email') }}" class="form-control" required>
                         </div>
                    </div>

                    <div class="form-group form-group-default" id="passwordGroup">
                        <label>{{ __('voyager::generic.password') }}</label>
                        <div class="controls">
                            <input type="password" name="password" placeholder="{{ __('voyager::generic.password') }}" class="form-control" required>
                        </div>
                    </div>

                    {{-- <div class="form-group" id="rememberMeGroup">
                        <div class="controls">
                        <input type="checkbox" name="remember" value="1"><span class="remember-me-text">{{ __('voyager::generic.remember_me') }}</span>
                        </div>
                    </div> --}}
                    
                    <button type="submit" class="btn btn-block login-button">
                        <span class="signingin hidden"><span class="voyager-refresh"></span> {{ __('voyager::login.loggingin') }}...</span>
                        <span class="signin">{{ __('voyager::generic.login') }}</span>
                    </button>

              </form>

              <div id="text-only">
                <p id="text-only-p">By Cunanan Catering</p>
              </div>

              <div style="clear:both"></div>

              @if(!$errors->isEmpty())
              <div class="alert alert-red">
                <ul class="list-unstyled">
                    @foreach($errors->all() as $err)
                    <li>{{ $err }}</li>
                    @endforeach
                </ul>
              </div>
              @endif

            </div> <!-- .login-container -->

        </div> <!-- .login-sidebar -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->
<script>
    var btn = document.querySelector('button[type="submit"]');
    var form = document.forms[0];
    var email = document.querySelector('[name="email"]');
    var password = document.querySelector('[name="password"]');
    btn.addEventListener('click', function(ev){
        if (form.checkValidity()) {
            btn.querySelector('.signingin').className = 'signingin';
            btn.querySelector('.signin').className = 'signin hidden';
        } else {
            ev.preventDefault();
        }
    });
    email.focus();
    document.getElementById('emailGroup').classList.add("focused");

    // Focus events for email and password fields
    email.addEventListener('focusin', function(e){
        document.getElementById('emailGroup').classList.add("focused");
    });
    email.addEventListener('focusout', function(e){
       document.getElementById('emailGroup').classList.remove("focused");
    });

    password.addEventListener('focusin', function(e){
        document.getElementById('passwordGroup').classList.add("focused");
    });
    password.addEventListener('focusout', function(e){
       document.getElementById('passwordGroup').classList.remove("focused");
    });

</script>
</body>
</html>
